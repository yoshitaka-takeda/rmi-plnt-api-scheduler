package id.co.indocyber.schedularRmi.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;

import id.co.indocyber.schedularRmi.config.ConstantVariabel;
import id.co.indocyber.schedularRmi.dao.MobSptaDao;
import id.co.indocyber.schedularRmi.dao.MobWeighbridgeResultDao;
import id.co.indocyber.schedularRmi.dao.TokenUserDao;
import id.co.indocyber.schedularRmi.entity.MobWeighbridgeResult;
import id.co.indocyber.schedularRmi.service.MobWeighBridgeResultSvc;
import id.co.indocyber.schedularRmi.service.NotificationSvc;

@Service
@Transactional
public class MobWeighBridgeResultSvcImpl implements MobWeighBridgeResultSvc {
	
	@Autowired
	MobWeighbridgeResultDao mobWeighbridgeResultDao;
	
	@Autowired
	MobSptaDao mobSptaDao;
	
	@Autowired
	TokenUserDao tokenUserDao;
	
	@Autowired
	NotificationSvc notificationSvc;
	
	HttpResponse response;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	HttpClient httpClient = HttpClientBuilder.create().build();
	
	JSONObject jobj = new JSONObject();
	
	@Override
	public void saveData() {
		
		String token = getToken();
		getWeighBridge(token);
		
	}
	
	public String getToken() {
		HttpGet request = new HttpGet(ConstantVariabel._tokenSync);
		
		String token = "";
		try {
			response = httpClient.execute(request);
			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(jobj.get("data").toString(), HashMap.class);
			token = yourHashMap.get("token").toString();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return token;
	}
	
	public void getWeighBridge(String token) {
		String url = ConstantVariabel._listWeighBridgeResult;
		HttpPost post = new HttpPost(url);
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("token", token));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(urlParameters));
			response = httpClient.execute(post);
			HttpEntity entity2 = response.getEntity();
			String responseString2 = EntityUtils.toString(entity2);
			jobj = new JSONObject(responseString2);
			
			JSONObject obj = jobj.getJSONObject("data");
			JSONArray joar = obj.getJSONArray("data");
			
			for (int i = 0; i < joar.length(); i++) {
				
				MobWeighbridgeResult mobWeighbridgeResult = new MobWeighbridgeResult();
				int error = 0;
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("document_num")) || joar.getJSONObject(i).get("document_num").equals("")) {
					error++;
					System.err.println("Error : no Spta kosong");
				} else {
					mobWeighbridgeResult.setDocumentNum(Integer.parseInt(joar.getJSONObject(i).getString("document_num")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("document_type")) || joar.getJSONObject(i).getString("document_type").equals("")) {
					error++;
					System.err.println("Error : document type kosong");
				} else {
					mobWeighbridgeResult.setDocumentType(joar.getJSONObject(i).getString("document_type"));
				}
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("mitra_code")) || joar.getJSONObject(i).getString("mitra_code").equals("")) {
					
					System.err.println("Error : mitra_code kosong");
				} else {
					mobWeighbridgeResult.setMitraCode(joar.getJSONObject(i).getString("mitra_code"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("mitra_name")) || joar.getJSONObject(i).getString("mitra_name").equals("")) {
					
					System.err.println("Error : item mitra_name kosong");
				} else {
					mobWeighbridgeResult.setMitraName(joar.getJSONObject(i).getString("mitra_name"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("vehicle_no")) || joar.getJSONObject(i).getString("vehicle_no").equals("")) {
					
					System.err.println("Error : vehicle_no kosong");
				} else {
					mobWeighbridgeResult.setVehicleNo(joar.getJSONObject(i).getString("vehicle_no"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("vehicle_type")) || joar.getJSONObject(i).get("vehicle_type").equals("")) {
					error++;
					System.err.println("Error : vehicle type kosong");
					
				} else {
					mobWeighbridgeResult.setVehicleType(joar.getJSONObject(i).getString("vehicle_type"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("driver_id")) || joar.getJSONObject(i).get("driver_id").equals("")) {
					error++;
					System.err.println("Error : driver_id  kosong");
					
				} else {
					mobWeighbridgeResult.setDriverId(joar.getJSONObject(i).getString("driver_id"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("driver_name")) || joar.getJSONObject(i).get("driver_name").equals("")) {
					error++;
					System.err.println("Error : driver_name kosong");
					
				} else {
					mobWeighbridgeResult.setDriverName(joar.getJSONObject(i).getString("driver_name"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farmer_code")) || joar.getJSONObject(i).get("farmer_code").equals("")) {
					error++;
					System.err.println("Error : farmer_code kosong");
					
				} else {
					mobWeighbridgeResult.setFarmerCode(joar.getJSONObject(i).getString("farmer_code"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farmer_name")) || joar.getJSONObject(i).get("farmer_name").equals("")) {
					error++;
					System.err.println("Error : farmer_name kosong");
					
				} else {
					mobWeighbridgeResult.setFarmerName(joar.getJSONObject(i).getString("farmer_name"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("contract_no_1")) || joar.getJSONObject(i).get("contract_no_1").equals("")) {
					error++;
					System.err.println("Error : contract_no_1 kosong");
					
				} else {
					mobWeighbridgeResult.setContractNo1(joar.getJSONObject(i).getString("contract_no_1"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("contract_no_2")) || joar.getJSONObject(i).get("contract_no_2").equals("")) {
					error++;
					System.err.println("Error : contract_no_2 kosong");
					
				} else {
					mobWeighbridgeResult.setContractNo2(joar.getJSONObject(i).getString("contract_no_2"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("date_in")) || joar.getJSONObject(i).get("date_in").equals("")) {
					error++;
					System.err.println("Error : date_in kosong");
					
				} else {
					mobWeighbridgeResult.setDateIn(dateFormat.parse(joar.getJSONObject(i).getString("date_in")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("time_in")) || joar.getJSONObject(i).get("time_in").equals("")) {
					error++;
					System.err.println("Error : time_in kosong");
					
				} else {
					mobWeighbridgeResult.setTimeIn(joar.getJSONObject(i).getString("time_in"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("date_out")) || joar.getJSONObject(i).get("date_out").equals("")) {
					error++;
					System.err.println("Error : date_out kosong");
					
				} else {
					mobWeighbridgeResult.setDateOut(dateFormat.parse(joar.getJSONObject(i).getString("date_out")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("time_out")) || joar.getJSONObject(i).get("time_out").equals("")) {
					error++;
					System.err.println("Error : time_out kosong");
					
				} else {
					mobWeighbridgeResult.setTimeOut(joar.getJSONObject(i).getString("time_out"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("status")) || joar.getJSONObject(i).get("status").equals("")) {
					error++;
					System.err.println("Error : status kosong");
					
				} else {
					mobWeighbridgeResult.setStatus(joar.getJSONObject(i).getString("status"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("monitoringPos")) || joar.getJSONObject(i).get("monitoringPos").equals("")) {
					
					System.err.println("Error : monitoringPos kosong");
					mobWeighbridgeResult.setMonitoringPos("");
				} else {
					mobWeighbridgeResult.setMonitoringPos(joar.getJSONObject(i).getString("monitoringPos"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("weigh_bruto")) || joar.getJSONObject(i).get("weigh_bruto").equals("")) {
					error++;
					System.err.println("Error : weigh_bruto kosong");
					
				} else {
					mobWeighbridgeResult.setWeighBruto(new BigDecimal(joar.getJSONObject(i).getString("weigh_bruto")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("weigh_netto")) || joar.getJSONObject(i).get("weigh_netto").equals("")) {
					error++;
					System.err.println("Error : weigh_netto kosong");
					
				} else {
					mobWeighbridgeResult.setWeighNetto(new BigDecimal(joar.getJSONObject(i).getString("weigh_netto")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("weigh_vehicle")) || joar.getJSONObject(i).get("weigh_vehicle").equals("")) {
					error++;
					System.err.println("Error : weigh_vehicle kosong");
					
				} else {
					mobWeighbridgeResult.setWeighVehicle(new BigDecimal(joar.getJSONObject(i).getString("weigh_vehicle")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("qty_weigho")) || joar.getJSONObject(i).get("qty_weigho").equals("")) {
					error++;
					System.err.println("Error : qty_weigho kosong");
					
				} else {
					mobWeighbridgeResult.setQtyWeigho(new BigDecimal(joar.getJSONObject(i).getString("qty_weigho")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("rafaksi")) || joar.getJSONObject(i).get("rafaksi").equals("")) {
					error++;
					System.err.println("Error : rafaksi kosong");
					
				} else {
					mobWeighbridgeResult.setRafaksi(new BigDecimal(joar.getJSONObject(i).getString("rafaksi")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("total_paid")) || joar.getJSONObject(i).get("total_paid").equals("")) {
					error++;
					System.err.println("Error : total_paid kosong");
					
				} else {
					mobWeighbridgeResult.setTotalPaid(new BigDecimal(joar.getJSONObject(i).getString("total_paid")));
				}
				
				System.err.println("================================================== Timbangan ==================================================");
				
				Integer id = mobWeighbridgeResult.getDocumentNum();
				if (id != 0 || id != null) {
					Long count = mobWeighbridgeResultDao.countWeighBridgeResultByid(id);
					if (count == 1) {
						// masuk edit
						MobWeighbridgeResult mobWeighbridgeResult2 = mobWeighbridgeResultDao.getWeighbridgeResult(mobWeighbridgeResult.getDocumentNum());
						System.out.println("masuk edit");
						if (mobWeighbridgeResult2 != null) {
							MobWeighbridgeResult mobWeighbridgeResult3 = mobWeighbridgeResultDao.getOne(mobWeighbridgeResult2.getId());
							if (mobWeighbridgeResult3 != null) {
								mobWeighbridgeResult3.setDocumentNum(mobWeighbridgeResult.getDocumentNum());
								mobWeighbridgeResult3.setDocumentType(mobWeighbridgeResult.getDocumentType());
								mobWeighbridgeResult3.setMitraCode(mobWeighbridgeResult.getMitraCode());
								mobWeighbridgeResult3.setMitraName(mobWeighbridgeResult.getMitraName());
								mobWeighbridgeResult3.setVehicleNo(mobWeighbridgeResult.getVehicleNo());
								mobWeighbridgeResult3.setVehicleType(mobWeighbridgeResult.getVehicleType());
								mobWeighbridgeResult3.setDriverId(mobWeighbridgeResult.getDriverId());
								mobWeighbridgeResult3.setDriverName(mobWeighbridgeResult.getDriverName());
								mobWeighbridgeResult3.setFarmerCode(mobWeighbridgeResult.getFarmerCode());
								mobWeighbridgeResult3.setFarmerName(mobWeighbridgeResult.getFarmerName());
								mobWeighbridgeResult3.setContractNo1(mobWeighbridgeResult.getContractNo1());
								mobWeighbridgeResult3.setContractNo2(mobWeighbridgeResult.getContractNo2());
								mobWeighbridgeResult3.setDateIn(mobWeighbridgeResult.getDateIn());
								mobWeighbridgeResult3.setTimeIn(mobWeighbridgeResult.getTimeIn());
								mobWeighbridgeResult3.setDateOut(mobWeighbridgeResult.getDateOut());
								mobWeighbridgeResult3.setTimeOut(mobWeighbridgeResult.getTimeOut());
								mobWeighbridgeResult3.setStatus(mobWeighbridgeResult.getStatus());
								mobWeighbridgeResult3.setMonitoringPos(mobWeighbridgeResult.getMonitoringPos());
								mobWeighbridgeResult3.setWeighBruto(mobWeighbridgeResult.getWeighBruto());
								mobWeighbridgeResult3.setWeighNetto(mobWeighbridgeResult.getWeighNetto());
								mobWeighbridgeResult3.setWeighVehicle(mobWeighbridgeResult.getWeighVehicle());
								mobWeighbridgeResult3.setQtyWeigho(mobWeighbridgeResult.getQtyWeigho());
								mobWeighbridgeResult3.setRafaksi(mobWeighbridgeResult.getRafaksi());
								mobWeighbridgeResult3.setTotalPaid(mobWeighbridgeResult.getTotalPaid());
								
								mobWeighbridgeResultDao.save(mobWeighbridgeResult3);
								System.out.println("data berhasil di update dengan no SPTA : " + mobWeighbridgeResult.getDocumentNum());
							}
						}
						
					} else if (count == 0) {
						// save
						
						if (error > 0) {
							System.err.println("gagal menyimpan dengan no SPTA :" + mobWeighbridgeResult.getDocumentNum());
						} else {
							try {
								mobWeighbridgeResultDao.save(mobWeighbridgeResult);
								System.out.println("data berhasil di save dengan no SPTA : " + mobWeighbridgeResult.getDocumentNum());
								
								String tknPetani = tokenUserDao.findTokenPetaniBySpta(mobWeighbridgeResult.getDocumentNum());
								String tknSupir = tokenUserDao.findTokenSupirBySpta(mobWeighbridgeResult.getDocumentNum());
								
								List<String> listTkn = new ArrayList<>();
								if (tknPetani != null) {
									listTkn.add(tknPetani);
									notificationSvc.notifWeighBridgeFcm(tknPetani, mobWeighbridgeResult.getDocumentNum());
								}
								if (tknSupir != null) {
									listTkn.add(tknSupir);
									notificationSvc.notifWeighBridgeFcm(tknSupir, mobWeighbridgeResult.getDocumentNum());
								}
								// notificationSvc.notifWeighBridge(listTkn, mobWeighbridgeResult.getDocumentNum());
							} catch (Exception e) {
								// TODO: handle exception
							}
						}
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
