package id.co.indocyber.schedularRmi.service.impl;

import id.co.indocyber.schedularRmi.config.ConstantVariabel;
import id.co.indocyber.schedularRmi.dao.MobFarmerDao;
import id.co.indocyber.schedularRmi.entity.MobFarmer;
import id.co.indocyber.schedularRmi.service.FarmerSvc;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ma.glasnost.orika.impl.generator.specification.Convert;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;

@Service
@Transactional
public class FarmerSvcImpl implements FarmerSvc {
	
	public Boolean init = false;
	
	@Autowired
	private MobFarmerDao mobFarmerDao;
	
	@SuppressWarnings("static-access")
	@Override
	public void saveData() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(ConstantVariabel._tokenSync);
		JSONObject jobj = new JSONObject();
		try {
			response = httpClient.execute(request);
			
			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			
			try {
				String url = ConstantVariabel._listFarmer;
				// HttpClient client2 = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				
				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("token", a));
				
				post.setEntity(new UrlEncodedFormEntity(urlParameters));
				
				response = httpClient.execute(post);
				HttpEntity entity2 = response.getEntity();
				String responseString2 = EntityUtils.toString(entity2);
				
				jobj = new JSONObject(responseString2);
				
				JSONObject obj = jobj.getJSONObject("data");
				JSONArray joar = obj.getJSONArray("data");
				
				for (int i = 0; i < joar.length(); i++) {
					MobFarmer farmer = new MobFarmer();
					int error = 0;
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("fullname"))) {
						error++;
						System.err.println("Error : fullname is null");
						farmer.setFullname("");
					} else {
						farmer.setFullname(joar.getJSONObject(i).getString("fullname"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("birthday"))) {
						error++;
						System.err.println("Error : birthday is null");
						farmer.setBirthday(null);
					} else {
						try {
							farmer.setBirthday(dateFormat.parse(joar.getJSONObject(i).getString("birthday")));
						} catch (Exception e) {
							// TODO: handle exception
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("place_of_birthday"))) {
						error++;
						System.err.println("Error : place_of_birthday is null");
						farmer.setPlaceOfBirthday("");
					} else {
						farmer.setPlaceOfBirthday(joar.getJSONObject(i).getString("place_of_birthday"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("address_type"))) {
						error++;
						System.err.println("Error : address_type is null");
						farmer.setAddressType("");
					} else {
						farmer.setAddressType(joar.getJSONObject(i).getString("address_type"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("address"))) {
						error++;
						System.err.println("Error : address is null");
						farmer.setAddress("");
					} else {
						farmer.setAddress(joar.getJSONObject(i).getString("address"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("rtrw"))) {
						error++;
						System.err.println("Error : rtrw is null");
						farmer.setRtrw("");
					} else {
						farmer.setRtrw(joar.getJSONObject(i).getString("rtrw"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("village"))) {
						error++;
						System.err.println("Error : village is null");
						farmer.setVillage("");
					} else {
						farmer.setVillage(joar.getJSONObject(i).getString("village"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("sub_district"))) {
						error++;
						System.err.println("Error : sub_district is null");
						farmer.setSubDistrict("");
					} else {
						farmer.setSubDistrict(joar.getJSONObject(i).getString("sub_district"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("district"))) {
						error++;
						System.err.println("Error : district is null");
						farmer.setDistrict("");
					} else {
						farmer.setDistrict(joar.getJSONObject(i).getString("district"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("city"))) {
						error++;
						System.err.println("Error : city is null");
						farmer.setCity("");
					} else {
						farmer.setCity(joar.getJSONObject(i).getString("city"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("province"))) {
						error++;
						System.err.println("Error : province is null");
						farmer.setProvince("");
					} else {
						farmer.setProvince(joar.getJSONObject(i).getString("province"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("zipcode"))) {
						error++;
						System.err.println("Error : zipcode is null");
						farmer.setZipcode("");
					} else {
						farmer.setZipcode(joar.getJSONObject(i).getString("zipcode"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("phone_num"))) {
						error++;
						System.err.println("Error : phone_num is null");
						farmer.setPhoneNum("");
					} else {
						farmer.setPhoneNum(joar.getJSONObject(i).getString("phone_num"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("email"))) {
						error++;
						System.err.println("Error : email is null");
						farmer.setEmail("");
					} else {
						farmer.setEmail(joar.getJSONObject(i).getString("email"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("status"))) {
						error++;
						System.err.println("Error : status is null");
						farmer.setStatus("");
					} else {
						farmer.setStatus(joar.getJSONObject(i).getString("status"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("gender"))) {
						error++;
						System.err.println("Error : gender is null");
						farmer.setGender("");
					} else {
						farmer.setGender(joar.getJSONObject(i).getString("gender"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("religion"))) {
						error++;
						System.err.println("Error : religion is null");
						farmer.setReligion("");
					} else {
						farmer.setReligion(joar.getJSONObject(i).getString("religion"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farm_type"))) {
						error++;
						System.err.println("Error : farm_type is null");
						farmer.setFarmType("");
					} else {
						farmer.setFarmType(joar.getJSONObject(i).getString("farm_type"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farmer_type"))) {
						error++;
						System.err.println("Error : farmer_type is null");
						farmer.setFarmerType("");
					} else {
						farmer.setFarmerType(joar.getJSONObject(i).getString("farmer_type"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("last_education"))) {
						error++;
						System.err.println("Error : last_education is null");
						farmer.setLastEducation("");
					} else {
						farmer.setLastEducation(joar.getJSONObject(i).getString("last_education"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("field_total"))) {
						error++;
						System.err.println("Error : field_total is null");
						farmer.setFieldTotal(new BigDecimal(0));
					} else {
						farmer.setFieldTotal(new BigDecimal(joar.getJSONObject(i).getString("field_total")));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("fund_type"))) {
						error++;
						System.err.println("Error : fund_type is null");
						farmer.setFundType("");
					} else {
						farmer.setFundType(joar.getJSONObject(i).getString("fund_type"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("business_partner_code"))) {
						error++;
						System.err.println("Error : business_partner_code is null");
						farmer.setBusinessPartnerCode("");
					} else {
						farmer.setBusinessPartnerCode(joar.getJSONObject(i).getString("business_partner_code"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("no_ktp"))) {
						error++;
						System.err.println("Error : no_ktp is null");
						farmer.setNoKtp("");
					} else {
						farmer.setNoKtp(joar.getJSONObject(i).getString("no_ktp"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("no_npwp"))) {
						error++;
						System.err.println("Error : no_npwp is null");
						farmer.setNoNpwp("");
					} else {
						farmer.setNoNpwp(joar.getJSONObject(i).getString("no_npwp"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farmer_id"))) {
						error++;
						System.err.println("Error : reference_code is null");
						farmer.setReferenceCode("");
					} else {
						farmer.setReferenceCode(joar.getJSONObject(i).getString("farmer_id"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("is_active"))) {
						error++;
						System.err.println("Error : is_active is null");
						farmer.setIsActive(new Byte("1"));
					} else {
						farmer.setIsActive(new Byte("1"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("created_by"))) {
						error++;
						System.err.println("Error : created_by is null");
						farmer.setCreatedBy("SYSTEM");
					} else {
						farmer.setCreatedBy("SYSTEM");
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("created_date"))) {
						error++;
						System.err.println("Error : created_date is null");
						farmer.setCreatedDate(new Date());
					} else {
						farmer.setCreatedDate(new Date());
						;
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("modified_by"))) {
						error++;
						System.err.println("Error : modified_by is null");
						farmer.setModifiedBy("");
					} else {
						farmer.setModifiedBy("");
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("modified_date"))) {
						error++;
						System.err.println("Error : modified_date is null");
						farmer.setModifiedDate(null);
					} else {
						farmer.setModifiedDate(null);
					}
					
					// if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("processed_by"))) {
					// error++;
					// System.err.println("Error : processed_by is null");
					// farmer.setp(new Byte("0"));
					// } else {
					// farmer.setIsActive(new Byte(joar.getJSONObject(i).getString("processed_by")));
					// }
					//
					// if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("processed_date"))) {
					// error++;
					// System.err.println("Error : processed_date is null");
					// farmer.setIsActive(new Byte("0"));
					// } else {
					// farmer.setIsActive(new Byte(joar.getJSONObject(i).getString("processed_date")));
					// }
					
					System.err.println("================================================== Petani ==================================================");
					
					Long count = mobFarmerDao.countDataFarmerByRefCode(farmer.getReferenceCode());
					
					if (count == 1) {
						MobFarmer farmer2 = mobFarmerDao.findOneByRefCode(farmer.getReferenceCode());
						if (farmer2 != null) {
							MobFarmer farmer3 = mobFarmerDao.findOne(farmer2.getId());
							if (farmer3 != null) {
								farmer3.setFullname(farmer.getFullname());
								farmer3.setBirthday(farmer.getBirthday());
								farmer3.setPlaceOfBirthday(farmer.getPlaceOfBirthday());
								farmer3.setAddressType(farmer.getAddressType());
								farmer3.setAddress(farmer.getAddress());
								farmer3.setRtrw(farmer.getRtrw());
								farmer3.setVillage(farmer.getVillage());
								farmer3.setSubDistrict(farmer.getSubDistrict());
								farmer3.setDistrict(farmer.getDistrict());
								farmer3.setCity(farmer.getCity());
								farmer3.setProvince(farmer.getProvince());
								farmer3.setZipcode(farmer.getZipcode());
								farmer3.setPhoneNum(farmer.getPhoneNum());
								farmer3.setEmail(farmer.getEmail());
								farmer3.setStatus(farmer.getStatus());
								farmer3.setGender(farmer.getGender());
								farmer3.setReligion(farmer.getReligion());
								farmer3.setFarmType(farmer.getFarmType());
								farmer3.setFarmerType(farmer.getFarmerType());
								farmer3.setLastEducation(farmer.getLastEducation());
								farmer3.setFieldTotal(farmer.getFieldTotal());
								farmer3.setFundType(farmer.getFundType());
								farmer3.setBusinessPartnerCode(farmer.getBusinessPartnerCode());
								farmer3.setNoKtp(farmer.getNoKtp());
								farmer3.setNoNpwp(farmer.getNoNpwp());
								farmer3.setIsActive(farmer.getIsActive());
								farmer3.setModifiedBy("SYSTEM");
								farmer3.setModifiedDate(new Date());
								mobFarmerDao.save(farmer3);
							}
						}
						System.err.println("Masuk edit id = " + farmer.getReferenceCode());
					} else if (count == 0) {
						if (error > 0) {
							System.err.println("Data Gagal Disimpan, untuk no ref = " + farmer.getReferenceCode());
						} else {
							mobFarmerDao.save(farmer);
							System.err.println("Data Tersimpan = " + farmer.getReferenceCode() + ", " + new Date().getTime());
						}
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void getNotification(String noHp) {
		// TODO Auto-generated method stub
		
	}
	
}
