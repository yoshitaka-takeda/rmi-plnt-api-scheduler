package id.co.indocyber.schedularRmi.service.impl;

import id.co.indocyber.schedularRmi.config.ConstantVariabel;
import id.co.indocyber.schedularRmi.dao.MobFarmerDao;
import id.co.indocyber.schedularRmi.dao.MobPaddyFieldDao;
import id.co.indocyber.schedularRmi.entity.MobFarmer;
import id.co.indocyber.schedularRmi.entity.MobPaddyField;
import id.co.indocyber.schedularRmi.service.PaddyFieldSvc;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ma.glasnost.orika.impl.generator.specification.Convert;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;

@Service
@Transactional
public class PaddyFieldSvcImpl implements PaddyFieldSvc {
	
	public Boolean init = false;
	
	@Autowired
	private MobFarmerDao mobFarmerDao;
	
	@Autowired
	private MobPaddyFieldDao mobPaddyFieldDao;
	
	@SuppressWarnings("static-access")
	@Override
	public void saveData() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(ConstantVariabel._tokenSync);
		JSONObject jobj = new JSONObject();
		
		try {
			response = httpClient.execute(request);
			
			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			
			try {
				String url = ConstantVariabel._listPaddyFieldCode;
				// HttpClient client2 = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				
				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("token", a));
				
				post.setEntity(new UrlEncodedFormEntity(urlParameters));
				
				response = httpClient.execute(post);
				HttpEntity entity2 = response.getEntity();
				String responseString2 = EntityUtils.toString(entity2);
				
				jobj = new JSONObject(responseString2);
				
				JSONObject obj = jobj.getJSONObject("data");
				JSONArray joar = obj.getJSONArray("data");
				
				for (int i = 0; i < joar.length(); i++) {
					MobPaddyField paddyField = new MobPaddyField();
					int error = 0;
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farmer_id")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : Farmer Id is null");
						paddyField.setFarmerId(0);
					} else {
						String ref = (String) joar.getJSONObject(i).get("farmer_id");
						System.err.println(ref);
						MobFarmer farmer = mobFarmerDao.findOneByRefCode(ref);
						try {
							if (farmer != null) {
								paddyField.setFarmerId(farmer.getId());
							} else {
								paddyField.setFarmerId(0);
								error++;
							}
						} catch (Exception e) {
							error++;
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farm_code")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : Farm Code is null");
						paddyField.setFarmCode("");
					} else {
						paddyField.setFarmCode(joar.getJSONObject(i).getString("farm_code"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("paddy_field_code")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : paddy_field_code is null");
						paddyField.setPaddyFieldCode("");
					} else {
						paddyField.setPaddyFieldCode(joar.getJSONObject(i).getString("paddy_field_code"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("line_num")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : line_num is null");
						paddyField.setLineNum(0);
					} else {
						paddyField.setLineNum(Integer.parseInt(joar.getJSONObject(i).getString("line_num")));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("field_area")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : field_area is null");
						paddyField.setFieldArea(BigDecimal.ZERO);
					} else {
						paddyField.setFieldArea(new BigDecimal(joar.getJSONObject(i).getString("field_area")));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("latitude")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : latitude is null");
						paddyField.setLatitude(BigDecimal.ZERO);
					} else {
						paddyField.setLatitude(BigDecimal.ZERO);
//						try {
//							paddyField.setLatitude(new BigDecimal(joar.getJSONObject(i).getString("latitude")));
//						} catch (Exception e) {
//							String tempLatitude = joar.getJSONObject(i).getString("latitude");
//							String[] arrLat = tempLatitude.split("\\,");
//							try {
//								paddyField.setLatitude(new BigDecimal(arrLat[0] + "." + arrLat[1]));
//							} catch (Exception err) {
//								error++;
//								System.err.println("format latitude salah");
//							}
//						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("longitude")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : longitude is null");
						paddyField.setLongitude(BigDecimal.ZERO);
					} else {
						paddyField.setLongitude(BigDecimal.ZERO);
//						try {
//							paddyField.setLongitude(new BigDecimal(joar.getJSONObject(i).getString("longitude")));
//						} catch (Exception e) {
//							String tempLatitude = joar.getJSONObject(i).getString("longitude");
//							String[] arrLat = tempLatitude.split("\\,");
//							try {
//								paddyField.setLatitude(new BigDecimal(arrLat[0] + "." + arrLat[1]));
//							} catch (Exception err) {
//								error++;
//								System.err.println("format longitude salah");
//							}
//						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("weight_estimation")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : weight_estimation is null");
						paddyField.setWeightEstimation(BigDecimal.ZERO);
					} else {
						try {
							paddyField.setWeightEstimation(new BigDecimal(joar.getJSONObject(i).getString("weight_estimation")));
						} catch (Exception e) {
							paddyField.setWeightEstimation(BigDecimal.ZERO);
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("sugarcane_type")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : sugarcane_type is null");
						paddyField.setSugarcaneType("");
					} else {
						paddyField.setSugarcaneType(joar.getJSONObject(i).getString("sugarcane_type"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("age_of_sugarcane")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : age_of_sugarcane is null");
						paddyField.setAgeOfSugarcane(0);
					} else {
						paddyField.setAgeOfSugarcane(Integer.parseInt(joar.getJSONObject(i).getString("age_of_sugarcane")));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("planting_date")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : planting_date is null");
						paddyField.setPlantingDate(null);
					} else {
						try {
							paddyField.setPlantingDate(dateFormat.parse(joar.getJSONObject(i).getString("planting_date")));	
						} catch (Exception e) {
							paddyField.setPlantingDate(null);
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("sugarcane_species")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : sugarcane_species is null");
						paddyField.setSugarcaneSpecies("");
					} else {
						paddyField.setSugarcaneSpecies(joar.getJSONObject(i).getString("sugarcane_species"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("sugarcane_score")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : sugarcane_score is null");
						paddyField.setSugarcaneScore(BigDecimal.ZERO);
					} else {
						paddyField.setSugarcaneScore(new BigDecimal(joar.getJSONObject(i).getString("sugarcane_score")));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("sugarcane_qty")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : sugarcane_qty is null");
						paddyField.setSugarcaneQty(BigDecimal.ZERO);
					} else {
						paddyField.setSugarcaneQty(new BigDecimal(joar.getJSONObject(i).getString("sugarcane_qty")));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("planting_period")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : planting_period is null");
						paddyField.setPlantingPeriod("");
					} else {
						paddyField.setPlantingPeriod(joar.getJSONObject(i).getString("planting_period"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("weigh_estimation")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : weigh_estimation is null");
						paddyField.setWeighEstimation(BigDecimal.ZERO);
					} else {
						try {
							paddyField.setWeighEstimation(new BigDecimal(joar.getJSONObject(i).getString("weigh_estimation")));
						} catch (Exception e) {
							paddyField.setWeighEstimation(BigDecimal.ZERO);
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("status")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : status is null");
						paddyField.setStatus("");
					} else {
						paddyField.setStatus(joar.getJSONObject(i).getString("status"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("reference_code")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : reference_code is null");
						paddyField.setReferenceCode("");
					} else {
						paddyField.setReferenceCode("");
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("is_active")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : is_active is null");
						paddyField.setIsActive(new Byte("1"));
					} else {
						try {
							paddyField.setIsActive(new Byte("1"));
						} catch (Exception e) {
							paddyField.setIsActive(new Byte("1"));
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("processed_by")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : processed_by is null");
						paddyField.setProcessedBy("SYSTEM");
					} else {
						paddyField.setProcessedBy("SYSTEM");
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("processed_date")) || joar.getJSONObject(i).equals("")) {
						error++;
						System.err.println("Error : processed_date is null");
						paddyField.setProcessedDate(new Date());
					} else {
						paddyField.setProcessedDate(new Date());
					}
					
					System.err.println("================================================== Petak ==================================================");
					
					Long count = mobPaddyFieldDao.countDataByFarmCodeAndPaddyFieldCode(paddyField.getFarmerId(), paddyField.getFarmCode(), paddyField.getPaddyFieldCode());
					System.err.println(count);
					if (count == 1) {
						MobPaddyField paddyField2 = mobPaddyFieldDao.findOneByFarmerFarmPaddyField(paddyField.getFarmerId(), paddyField.getFarmCode(), paddyField.getPaddyFieldCode());
						if (paddyField2 != null) {
							MobPaddyField paddyField3 = mobPaddyFieldDao.findOne(paddyField2.getId());
							if (paddyField3 != null) {
								paddyField3.setLineNum(paddyField.getLineNum());
								paddyField3.setFieldArea(paddyField.getFieldArea());
								paddyField3.setLatitude(paddyField.getLatitude());
								paddyField3.setLongitude(paddyField.getLongitude());
								paddyField3.setWeightEstimation(paddyField.getWeightEstimation());
								paddyField3.setSugarcaneType(paddyField.getSugarcaneType());
								paddyField3.setAgeOfSugarcane(paddyField.getAgeOfSugarcane());
								paddyField3.setPlantingDate(paddyField.getPlantingDate());
								paddyField3.setSugarcaneSpecies(paddyField.getSugarcaneSpecies());
								paddyField3.setSugarcaneScore(paddyField.getSugarcaneScore());
								paddyField3.setSugarcaneQty(paddyField.getSugarcaneQty());
								paddyField3.setPlantingPeriod(paddyField.getPlantingPeriod());
								paddyField3.setWeighEstimation(paddyField.getWeighEstimation());
								paddyField3.setStatus(paddyField.getStatus());
								paddyField3.setReferenceCode(paddyField.getReferenceCode());
								paddyField3.setIsActive(paddyField.getIsActive());
								paddyField3.setProcessedBy(paddyField.getProcessedBy());
								paddyField3.setProcessedDate(paddyField.getProcessedDate());
								System.err.println(paddyField.getFieldArea()+" <------");
								System.err.println(paddyField3.getFieldArea()+" <------");
								mobPaddyFieldDao.save(paddyField3);
							}
						}
					} else if (count == 0) {
						if (error > 0) {
							System.err.println("Data Gagal Disimpan, untuk no petak = " + paddyField.getPaddyFieldCode());
						} else {
							mobPaddyFieldDao.save(paddyField);
							// getNotification(spt.getPhone());
							// notificationSvc.getNotification(spt.getPhone(), spt.getSptNo(), 1);
							System.err.println("Data Tersimpan = " + paddyField.getFarmCode() + ", " + new Date().getTime());
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void getNotification(String noHp) {
		// TODO Auto-generated method stub
		
	}
	
}
