package id.co.indocyber.schedularRmi.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.gson.Gson;

import id.co.indocyber.schedularRmi.config.ConstantVariabel;
import id.co.indocyber.schedularRmi.dao.MobSptaDao;
import id.co.indocyber.schedularRmi.dao.TokenUserDao;
import id.co.indocyber.schedularRmi.entity.MobSpta;
import id.co.indocyber.schedularRmi.service.MobSptaWBResultSvc;
import id.co.indocyber.schedularRmi.service.NotificationSvc;

public class MobSptaWBResultSvcImpl implements MobSptaWBResultSvc {
	public Boolean init = false;
	
	@Autowired
	MobSptaDao mobSptaDao;
	
	@Autowired
	TokenUserDao tokenUserDao;
	
	@Autowired
	NotificationSvc notificationSvc;
	
	HttpResponse response;
	
	SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	
	HttpClient httpClient = HttpClientBuilder.create().build();
	
	JSONObject jobj = new JSONObject();
	
	@Override
	public void saveData() {
		
		String token = getToken();
		getListSpta(token);
		System.err.println("scheduller 2 jalan");
	}
	
	public String getToken() {
		HttpGet request = new HttpGet(ConstantVariabel._tokenSync);
		
		String token = "";
		try {
			response = httpClient.execute(request);
			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(jobj.get("data").toString(), HashMap.class);
			token = yourHashMap.get("token").toString();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return token;
	}
	
	public void getListSpta(String token) {
		String url = ConstantVariabel._listSptaWeightBridge;
		HttpPost post = new HttpPost(url);
		
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("token", token));
		
		try {
			post.setEntity(new UrlEncodedFormEntity(urlParameters));
			response = httpClient.execute(post);
			HttpEntity entity2 = response.getEntity();
			String responseString2 = EntityUtils.toString(entity2);
			jobj = new JSONObject(responseString2);
			
			JSONObject obj = jobj.getJSONObject("data");
			JSONArray joar = obj.getJSONArray("data");
			
			// if (init == false) {
			// GoogleCredentials credentials;
			// try {
			// credentials = GoogleCredentials.fromStream(
			// // new FileInputStream("C:/Users/User/Downloads/sparepartfactory-4d619-firebase-adminsdk-71jvb-f5547e041b.json"))
			// new URL("http://124.158.178.196:8080/bmm-asset/berkahmm-78460-firebase-adminsdk-xwo8d-d9e22c8826.json").openStream()).createScoped(
			// Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
			// FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(credentials).setDatabaseUrl("https://berkahmm-78460.firebaseio.com").build();
			//
			// FirebaseApp.initializeApp(options);
			// init = true;
			// } catch (MalformedURLException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// } catch (IOException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			// }
			
			for (int i = 0; i < joar.length(); i++) {
				// proses save
				MobSpta mobSpta = new MobSpta();
				int error = 0;
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("spta_num")) || joar.getJSONObject(i).get("spta_num").equals("")) {
					error++;
					System.err.println("Error : no Spta kosong");
				} else {
					mobSpta.setSptaNum(Integer.parseInt(joar.getJSONObject(i).getString("spta_num")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("farmer_code")) || joar.getJSONObject(i).getString("farmer_code").equals("")) {
					error++;
					System.err.println("Error : no farmer kosong");
					mobSpta.setFarmerCode("");
				} else {
					mobSpta.setFarmerCode(joar.getJSONObject(i).getString("farmer_code"));
				}
				
				// iooo
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("contract_no1")) || joar.getJSONObject(i).getString("contract_no1").equals("")) {
					error++;
					System.err.println("Error : contract_no1 kosong");
					mobSpta.setContractNo1("");
				} else {
					mobSpta.setContractNo1(joar.getJSONObject(i).getString("contract_no1"));
				}
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("contract_no2")) || joar.getJSONObject(i).getString("contract_no2").equals("")) {
					error++;
					System.err.println("Error : contract_no2 kosong");
				} else {
					mobSpta.setContractNo2(joar.getJSONObject(i).getString("contract_no2"));
				}
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("farmer_type")) || joar.getJSONObject(i).getString("farmer_type").equals("")) {
					error++;
					System.err.println("Error : farmer_type kosong");
				} else {
					mobSpta.setFarmerType(joar.getJSONObject(i).getString("farmer_type"));
				}
				// io
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("farm_code")) || joar.getJSONObject(i).getString("farm_code").equals("")) {
					error++;
					System.err.println("farm_code kosong");
				} else {
					mobSpta.setFarmCode(joar.getJSONObject(i).getString("farm_code"));
				}
				
				// io
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("coop_name")) || joar.getJSONObject(i).getString("coop_name").equals("")) {
					error++;
					System.err.println("Error : coop_name kosong");
				} else {
					mobSpta.setCoopName(joar.getJSONObject(i).getString("coop_name"));
				}
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("monitoring_post")) || joar.getJSONObject(i).getString("monitoring_post").equals("")
						|| joar.getJSONObject(i).getString("monitoring_post") == null) {
					
					System.err.println("Error : monitoring_post kosong");
					mobSpta.setMonitoringPos("");
				} else {
					mobSpta.setMonitoringPos(joar.getJSONObject(i).getString("monitoring_post"));
				}
				// oui
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("qty")) || joar.getJSONObject(i).getString("qty").equals("")) {
					System.err.println("Error : qty kosong");
					mobSpta.setQty(null);
				} else {
					mobSpta.setQty(new BigDecimal(joar.getJSONObject(i).getString("qty")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("status")) || joar.getJSONObject(i).getString("status").equals("")) {
					System.err.println("Error : status kosong");
					mobSpta.setStatus(null);
				} else {
					mobSpta.setStatus(joar.getJSONObject(i).getString("status"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("vehicle_no")) || joar.getJSONObject(i).getString("vehicle_no").equals("")) {
					System.err.println("Error : vehicle_no kosong");
					mobSpta.setVehicleNo(null);
				} else {
					mobSpta.setVehicleNo(joar.getJSONObject(i).getString("vehicle_no"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("driver_id")) || joar.getJSONObject(i).getString("driver_id").equals("")) {
					System.err.println("Error : driver_id kosong");
					mobSpta.setDriverId(null);
				} else {
					mobSpta.setDriverId(Integer.parseInt(joar.getJSONObject(i).getString("driver_id")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("vehicle_type")) || joar.getJSONObject(i).getString("vehicle_type").equals("")) {
					System.err.println("Error : driver_id kosong");
					mobSpta.setVehicleType(null);
				} else {
					mobSpta.setVehicleType(Integer.parseInt(joar.getJSONObject(i).getString("vehicle_type")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("document_date")) || joar.getJSONObject(i).getString("document_date").equals("")) {
					System.err.println("Error : document_date kosong");
					mobSpta.setDocumentDate(null);
				} else {
					mobSpta.setDocumentDate(dateFormat.parse(joar.getJSONObject(i).getString("document_date")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("expired_date")) || joar.getJSONObject(i).getString("expired_date").equals("")) {
					System.err.println("Error : expired_date kosong");
					mobSpta.setExpiredDate(null);
				} else {
					mobSpta.setExpiredDate(dateFormat.parse(joar.getJSONObject(i).getString("expired_date")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("rendemen")) || joar.getJSONObject(i).get("rendemen").equals("") || joar.getJSONObject(i).getString("rendemen") == null) {
					System.err.println("Error : rendemen kosong");
					mobSpta.setRendemen(null);
				} else {
					mobSpta.setRendemen(new BigDecimal(joar.getJSONObject(i).getString("rendemen")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("brix")) || joar.getJSONObject(i).get("brix").equals("") || joar.getJSONObject(i).getString("brix") == null) {
					System.err.println("Error : brix kosong");
					mobSpta.setBrix(null);
				} else {
					mobSpta.setBrix(new BigDecimal(joar.getJSONObject(i).get("brix").toString()));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("pol")) || joar.getJSONObject(i).get("pol").equals("") || joar.getJSONObject(i).getString("pol") == null) {
					System.err.println("Error : pol kosong");
					mobSpta.setPol(null);
				} else {
					mobSpta.setPol(new BigDecimal(joar.getJSONObject(i).get("pol").toString()));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("source")) || joar.getJSONObject(i).getString("source").equals("")) {
					System.err.println("Error : source kosong");
					mobSpta.setSource(null);
				} else {
					mobSpta.setSource(joar.getJSONObject(i).getString("source"));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("base_entry")) || joar.getJSONObject(i).getString("base_entry").equals("")) {
					System.err.println("Error : base_entry kosong");
					mobSpta.setBaseEntry(null);
				} else {
					mobSpta.setBaseEntry(Integer.parseInt(joar.getJSONObject(i).getString("base_entry")));
				}
				
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("base_line")) || joar.getJSONObject(i).getString("base_line").equals("")) {
					System.err.println("Error : base_line kosong");
					mobSpta.setBaseLine(null);
				} else {
					mobSpta.setBaseLine(Integer.parseInt(joar.getJSONObject(i).getString("base_line")));
				}
				if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).getString("is_active")) || joar.getJSONObject(i).getString("is_active").equals("")) {
					System.err.println("Error : is_active kosong");
					byte temp = 1;
					mobSpta.setIsActive(new Byte("1"));
				} else {
					Integer temp = Integer.parseInt(joar.getJSONObject(i).getString("is_active"));
					byte g = temp.byteValue();
					mobSpta.setIsActive(new Byte("1"));
				}
				mobSpta.setCreatedBy("SYSTEM");
				mobSpta.setCreatedDate(new Date());
				mobSpta.setModifiedBy("SYSTEM");
				mobSpta.setModifiedDate(new Date());
				
				System.err.println("================================================== SPTA ==================================================");
				
				Long count = mobSptaDao.countSptaByid(mobSpta.getSptaNum());
				if (count == 1) {
					if (error > 0) {
						System.err.println("data gagal terupdate dengan no Spta : " + mobSpta.getSptaNum());
					} else {
						MobSpta tempSpta = mobSptaDao.getSptaBynoSpta(mobSpta.getSptaNum());
						if (tempSpta != null) {
							tempSpta.setFarmCode(mobSpta.getFarmCode());
							tempSpta.setContractNo1(mobSpta.getContractNo1());
							tempSpta.setContractNo2(mobSpta.getContractNo2());
							tempSpta.setFarmerType(mobSpta.getFarmerType());
							tempSpta.setFarmCode(mobSpta.getFarmCode());
							tempSpta.setCoopName(mobSpta.getCoopName());
							tempSpta.setQty(mobSpta.getQty());
							tempSpta.setStatus(mobSpta.getStatus());
							
							tempSpta.setVehicleNo(mobSpta.getVehicleNo());
							tempSpta.setDriverId(mobSpta.getDriverId());
							tempSpta.setVehicleType(mobSpta.getVehicleType());
							
							tempSpta.setDocumentDate(mobSpta.getDocumentDate());
							tempSpta.setExpiredDate(mobSpta.getExpiredDate());
							tempSpta.setMonitoringPos(mobSpta.getMonitoringPos());
							tempSpta.setRendemen(mobSpta.getRendemen());
							tempSpta.setBrix(mobSpta.getBrix());
							tempSpta.setPol(mobSpta.getPol());
							tempSpta.setSource(mobSpta.getSource());
							tempSpta.setBaseEntry(mobSpta.getBaseEntry());
							tempSpta.setBaseLine(mobSpta.getBaseLine());
							tempSpta.setIsActive(new Byte("1"));
							mobSptaDao.save(tempSpta);
							System.err.println("data terupdate - kan : " + mobSpta.getSptaNum());
						}
						
						// notif
					}
				} else if (count == 0) {
					if (error > 0) {
						System.err.println("data gagal tersimpan dengan no Spta : " + mobSpta.getSptaNum());
					} else {
						mobSptaDao.save(mobSpta);
						System.err.println("data tersimpan : " + mobSpta.getSptaNum());
						// notif
						String tkn = tokenUserDao.findTokenPetani(mobSpta.getFarmerCode());
						if (tkn != null) {
							// notificationSvc.notifSpta(tkn, mobSpta.getSptaNum());
							notificationSvc.notifSptaFcm(tkn, mobSpta.getSptaNum());
						}
					}
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
}
