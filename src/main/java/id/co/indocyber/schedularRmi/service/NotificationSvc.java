package id.co.indocyber.schedularRmi.service;

import java.util.List;
import java.util.Map;

public interface NotificationSvc {

	public void notifSpta(String token, Integer sptaNum);
	
	public void notifWeighBridge(List<String> token, Integer sptaNum);
	
	public Map<String, Object> notifSptaFcm(String token, Integer sptaNum);
	
	public Map<String, Object> notifWeighBridgeFcm(String token, Integer sptaNum);
}
