package id.co.indocyber.schedularRmi.service.impl;

import id.co.indocyber.schedularRmi.config.ConstantVariabel;
import id.co.indocyber.schedularRmi.config.LongLatSet;
import id.co.indocyber.schedularRmi.dao.MobFarmDao;
import id.co.indocyber.schedularRmi.dao.MobFarmerDao;
import id.co.indocyber.schedularRmi.entity.MobFarm;
import id.co.indocyber.schedularRmi.entity.MobFarmer;
import id.co.indocyber.schedularRmi.service.FarmSvc;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import ma.glasnost.orika.impl.generator.specification.Convert;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.google.gson.Gson;

@Service
@Transactional
public class FarmSvcImpl implements FarmSvc {
	
	public Boolean init = false;
	
	@Autowired
	private MobFarmDao mobFarmDao;
	
	@Autowired
	private MobFarmerDao mobFarmerDao;
	
	@SuppressWarnings("static-access")
	@Override
	public void saveData() {
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		HttpResponse response;
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(ConstantVariabel._tokenSync);
		JSONObject jobj = new JSONObject();
		
		try {
			response = httpClient.execute(request);
			
			HttpEntity entity = response.getEntity();
			String responseString;
			responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			@SuppressWarnings("unchecked")
			HashMap<String, Object> yourHashMap = new Gson().fromJson(jobj.get("data").toString(), HashMap.class);
			String a = yourHashMap.get("token").toString();
			
			try {
				String url = ConstantVariabel._listFarm;
				// HttpClient client2 = new DefaultHttpClient();
				HttpPost post = new HttpPost(url);
				
				List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
				urlParameters.add(new BasicNameValuePair("token", a));
				
				post.setEntity(new UrlEncodedFormEntity(urlParameters));
				
				response = httpClient.execute(post);
				HttpEntity entity2 = response.getEntity();
				String responseString2 = EntityUtils.toString(entity2);
				
				jobj = new JSONObject(responseString2);
				
				JSONObject obj = jobj.getJSONObject("data");
				JSONArray joar = obj.getJSONArray("data");
				
				// if(init==false){
				// GoogleCredentials credentials;
				// try {
				// credentials = GoogleCredentials.fromStream(
				// //new FileInputStream("C:/Users/User/Downloads/sparepartfactory-4d619-firebase-adminsdk-71jvb-f5547e041b.json"))
				// new URL("http://35.240.160.9:8080/bmm-asset/berkahmm-78460-firebase-adminsdk-xwo8d-d9e22c8826.json").openStream())
				// .createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
				// FirebaseOptions options = new FirebaseOptions.Builder()
				// .setCredentials(credentials)
				// .setDatabaseUrl("https://berkahmm-78460.firebaseio.com").build();
				//
				// FirebaseApp.initializeApp(options);
				// init=true;
				// } catch (MalformedURLException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// } catch (IOException e) {
				// // TODO Auto-generated catch block
				// e.printStackTrace();
				// }
				// }
				
				for (int i = 0; i < joar.length(); i++) {
					
					// cek bener ga dulu baru jalan
					MobFarm farm = new MobFarm();
					int error = 0;
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farmer_id"))) {
						error++;
						System.err.println("Error : Farmer Id is null");
						farm.setFarmerId(0);
					} else {
						String ref = (String) joar.getJSONObject(i).get("farmer_id");
						System.err.println(ref);
						MobFarmer farmer = mobFarmerDao.findOneByRefCode(ref);
						try {
							if (farmer != null) {
								farm.setFarmerId(farmer.getId());
							} else {
								farm.setFarmerId(0);
								error++;
							}
						} catch (Exception e) {
							error++;
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farm_code"))) {
						error++;
						System.err.println("Error : Farm Code is null");
						farm.setFarmCode("");
					} else {
						farm.setFarmCode(joar.getJSONObject(i).getString("farm_code"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("field_area")) || StringUtils.isEmpty(joar.getJSONObject(i).get("field_area")) || joar.getJSONObject(i).equals("")) {
						
						System.err.println("Error : Field Area is null");
						farm.setFieldArea(BigDecimal.ZERO);
					} else {
						farm.setFieldArea(new BigDecimal(joar.getJSONObject(i).getString("field_area")));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("village"))) {
						error++;
						System.err.println("Error : Village is null");
						farm.setVillage("");
					} else {
						farm.setVillage(joar.getJSONObject(i).getString("village"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("sub_district"))) {
						error++;
						System.err.println("Error : Sub Dis is null");
						farm.setSubDistrict("");
					} else {
						farm.setSubDistrict(joar.getJSONObject(i).getString("sub_district"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("district"))) {
						error++;
						System.err.println("Error : Dis is null");
						farm.setDistrict("");
					} else {
						farm.setDistrict(joar.getJSONObject(i).getString("district"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("province"))) {
						error++;
						System.err.println("Error : Prov is null");
						farm.setProvince("");
					} else {
						farm.setProvince(joar.getJSONObject(i).getString("province"));
					}
					System.err.println(joar.getJSONObject(i).get("latitude"));
					System.err.println("-------> " + longLat((String) joar.getJSONObject(i).get("latitude")));
					// System.err.println(farmer.getId());
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("latitude")) || StringUtils.isEmpty(joar.getJSONObject(i).get("latitude"))) {
						error++;
						System.err.println("Error : Lat is null");
						farm.setLatitude(BigDecimal.ZERO);
					} else {
						try {
							// farm.setLatitude(new BigDecimal(arrLat[0] + "." + arrLat[1]));
							farm.setLatitude(BigDecimal.ZERO);
						} catch (Exception e) {
							String tempLatitude = joar.getJSONObject(i).getString("latitude");
							String[] arrLat = tempLatitude.split("\\,");
							try {
								// farm.setLatitude(new BigDecimal(arrLat[0] + "." + arrLat[1]));
								farm.setLatitude(BigDecimal.ZERO);
							} catch (Exception err) {
								error++;
								System.err.println("format latitude salah");
							}
						}
					}
					System.err.println("-------> " + longLat((String) joar.getJSONObject(i).get("latitude")));
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("longitude")) || StringUtils.isEmpty(joar.getJSONObject(i).get("longitude"))) {
						error++;
						System.err.println("Error : Long is null");
						farm.setLongitude(BigDecimal.ZERO);
					} else {
						try {
							// farm.setLatitude(new BigDecimal(arrLat[0] + "." + arrLat[1]));
							farm.setLatitude(BigDecimal.ZERO);
						} catch (Exception e) {
							String tempLatitude = joar.getJSONObject(i).getString("longitude");
							String[] arrLat = tempLatitude.split("\\,");
							try {
								// farm.setLatitude(new BigDecimal(arrLat[0] + "." + arrLat[1]));
								farm.setLatitude(BigDecimal.ZERO);
							} catch (Exception err) {
								error++;
								System.err.println("format longitude salah");
							}
						}
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farm_main_type"))) {
						error++;
						System.err.println("Error : Farm Main is null");
						farm.setFarmMainType("");
					} else {
						farm.setFarmMainType(joar.getJSONObject(i).getString("farm_main_type"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("farm_sub_type"))) {
						error++;
						System.err.println("Error : Sub Type is null");
						farm.setFarmSubType("");
					} else {
						farm.setFarmSubType(joar.getJSONObject(i).getString("farm_sub_type"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("reference_code"))) {
						error++;
						System.err.println("Error : Ref is null");
						farm.setReferenceCode("");
					} else {
						farm.setReferenceCode(joar.getJSONObject(i).getString("reference_code"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("status"))) {
						error++;
						System.err.println("Error : Status is null");
						farm.setStatus("");
					} else {
						farm.setStatus(joar.getJSONObject(i).getString("status"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("is_active"))) {
						error++;
						System.err.println("Error : Activ is null");
						farm.setIsActive(new Byte("1"));
					} else {
						farm.setIsActive(new Byte("1"));
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("processed_by"))) {
						
						System.err.println("Error : Proc By is null");
						farm.setProcessedBy("SYSTEM");
					} else {
						farm.setProcessedBy("SYSTEM");
					}
					
					if (joar.getJSONObject(i).NULL.equals(joar.getJSONObject(i).get("processed_date"))) {
						
						System.err.println("Error : Proc Date is null");
						farm.setProcessedDate(new Date());
					} else {
						farm.setProcessedDate(new Date());
					}
					
					System.err.println("================================================== Kebun ==================================================");
					
					// System.err.println(farm.toString());
					Long count = mobFarmDao.countDataByFarmCode(farm.getFarmCode());
					if (count == 1) {
						String ref = (String) joar.getJSONObject(i).get("farmer_id");
						MobFarmer farmer = mobFarmerDao.findOneByRefCode(ref);
						MobFarm farm2 = mobFarmDao.findOneByFarmCode(farmer.getId(), farm.getFarmCode());
						if (farm2 != null) {
							farm2.setFieldArea(farm.getFieldArea());
							farm2.setVillage(farm.getVillage());
							farm2.setSubDistrict(farm.getSubDistrict());
							farm2.setDistrict(farm.getDistrict());
							farm2.setProvince(farm.getProvince());
							farm2.setLatitude(farm.getLatitude());
							farm2.setLongitude(farm.getLongitude());
							farm2.setFarmMainType(farm.getFarmMainType());
							farm2.setFarmSubType(farm.getFarmSubType());
							farm2.setReferenceCode(farm.getReferenceCode());
							farm2.setStatus(farm.getStatus());
							farm2.setIsActive(farm.getIsActive());
							mobFarmDao.save(farm2);
						}
						System.err.println("Masuk edit");
					} else if (count == 0) {
						if (error > 0) {
							System.err.println("Data Gagal Disimpan, untuk no kebun = " + farm.getFarmCode());
						} else {
							mobFarmDao.save(farm);
							System.err.println("Data Tersimpan = " + farm.getFarmCode() + ", " + new Date().getTime());
						}
					}
					
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	@Override
	public void getNotification(String noHp) {
		// TODO Auto-generated method stub
		
	}
	
	public String longLat(String data) {
		String longlat = "";
		try {
			String[] strings = data.split(",");
			longlat = strings[0] + "." + strings[1];
		} catch (Exception e) {
			try {
				String[] strings = data.split(".");
				longlat = strings[0] + "." + strings[1];
			} catch (Exception e2) {
				longlat = "0.0";
			}
		}
		return longlat;
	}
	
}
