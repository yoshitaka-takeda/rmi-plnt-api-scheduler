package id.co.indocyber.schedularRmi.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.common.collect.Lists;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.AndroidConfig;
import com.google.firebase.messaging.AndroidNotification;
import com.google.firebase.messaging.BatchResponse;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingException;
import com.google.firebase.messaging.Message;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;

import id.co.indocyber.schedularRmi.config.ConstantVariabel;
import id.co.indocyber.schedularRmi.service.NotificationSvc;

@Service
@Transactional
public class NotificationSvcImpl implements NotificationSvc {
	
	Boolean init = false;
	
	public void getFirebase(Boolean init) {
		if (init == false) {
			GoogleCredentials credentials;
			try {
				credentials = GoogleCredentials.fromStream(
				
				new URL(ConstantVariabel._URL).openStream()).createScoped(Lists.newArrayList("https://www.googleapis.com/auth/cloud-platform"));
				FirebaseOptions options = new FirebaseOptions.Builder().setCredentials(credentials).setDatabaseUrl(ConstantVariabel._DB).build();
				
				if (FirebaseApp.getApps().size() == 0)
					FirebaseApp.initializeApp(options);
				init = true;
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void notifSpta(String token, Integer sptaNum) {
		getFirebase(init);
		String sptaNum1 = sptaNum + "";
		String msg = "Anda mendapatkan spta baru dengan no : " + sptaNum1 + ".";
		Message message = Message.builder().setNotification(new Notification("RMI", msg)).setToken(token)
				.setAndroidConfig(AndroidConfig.builder().setNotification(AndroidNotification.builder().build()).build()).putData("noSurat", sptaNum1).build();
		try {
			String response = FirebaseMessaging.getInstance().send(message);
			System.out.println("pesan berhasil di kirim " + response);
		} catch (FirebaseMessagingException e) {
			e.printStackTrace();
			System.out.println("Notifikasi Gagal Dikirim");
		}
	}
	
	@Override
	public void notifWeighBridge(List<String> token, Integer sptaNum) {
		getFirebase(init);
		String sptaNum1 = sptaNum + "";
		String msg = "Hasil timbangan dengan no Spta :" + sptaNum1 + " sudah dapat dilihat.";
		
		MulticastMessage messageSales = MulticastMessage.builder().setNotification(new Notification("RMI", msg)).addAllTokens(token)
				.setAndroidConfig(AndroidConfig.builder().setNotification(AndroidNotification.builder().setColor("#f15a24").build()).build()).putData("no Spta", sptaNum1).build();
		try {
			BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(messageSales);
			System.out.println("pesan berhasil di kirim " + response);
		} catch (FirebaseMessagingException e) {
			e.printStackTrace();
			System.out.println("Notifikasi Gagal Dikirim");
		}
		
	}

	@Override
	public Map<String, Object> notifSptaFcm(String token, Integer sptaNum) {
		HashMap<String, Object> result = new HashMap<>();
		try {
			String body = ConstantVariabel._BodySpta + sptaNum + ".";
			HttpResponse response;
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost request = new HttpPost(ConstantVariabel._URLPost);
			JSONObject jobj = new JSONObject();
			StringEntity params = new StringEntity("{" 
					+ "\"to\" : \"" + token + "\"," 
					+ "\"collapse_key\" : \"type_a\"," 
					+ "\"notification\" : {" 
					+ "\"body\" : \"" + body + "\"," 
					+ "\"title\": \"" + ConstantVariabel._Title + "\"" 
					+ "}," 
					+ "\"data\" : {" 
					+ "\"body\" : \"Body of Your Notification in Data\"," 
					+ "\"title\": \"Title of Your Notification in Title\","
					+ "\"key_1\" : \"Value for key_1\"," 
					+ "\"key_2\" : \"Value for key_2\"" 
					+ "}" 
					+ "}");
			request.addHeader("Content-Type", ConstantVariabel._ContentType);
			request.addHeader("Authorization", ConstantVariabel._Authorization);
			request.setEntity(params);
			response = httpClient.execute(request);
			
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			Integer success = jobj.getInt("success");
			Integer failure = jobj.getInt("failure");
			if (success == 1 && failure == 0) {
				System.out.println("Sukses bro..");
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} else {
				System.out.println("Gagal soon..");
				result.put("status", 0);
				result.put("message", "Gagal");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e.getMessage());
			result.put("Object", null);
		}
		return result;
	}

	@Override
	public Map<String, Object> notifWeighBridgeFcm(String token, Integer sptaNum) {
		HashMap<String, Object> result = new HashMap<>();
		try {
			String body = ConstantVariabel._BodyWbResult + sptaNum + " sudah dapat dilihat.";
			HttpResponse response;
			HttpClient httpClient = HttpClientBuilder.create().build();
			HttpPost request = new HttpPost(ConstantVariabel._URLPost);
			JSONObject jobj = new JSONObject();
			StringEntity params = new StringEntity("{" 
					+ "\"to\" : \"" + token + "\"," 
					+ "\"collapse_key\" : \"type_a\"," 
					+ "\"notification\" : {" 
					+ "\"body\" : \"" + body + "\"," 
					+ "\"title\": \"" + ConstantVariabel._Title + "\"" 
					+ "}," 
					+ "\"data\" : {" 
					+ "\"body\" : \"Body of Your Notification in Data\"," 
					+ "\"title\": \"Title of Your Notification in Title\","
					+ "\"key_1\" : \"Value for key_1\"," 
					+ "\"key_2\" : \"Value for key_2\"" 
					+ "}" 
					+ "}");
			request.addHeader("Content-Type", ConstantVariabel._ContentType);
			request.addHeader("Authorization", ConstantVariabel._Authorization);
			request.setEntity(params);
			response = httpClient.execute(request);
			
			HttpEntity entity = response.getEntity();
			String responseString = EntityUtils.toString(entity);
			jobj = new JSONObject(responseString);
			Integer success = jobj.getInt("success");
			Integer failure = jobj.getInt("failure");
			if (success == 1 && failure == 0) {
				System.out.println("Sukses bro..");
				result.put("status", 1);
				result.put("message", "Sukses");
				result.put("Object", null);
			} else {
				System.out.println("Gagal soon..");
				result.put("status", 0);
				result.put("message", "Gagal");
				result.put("Object", null);
			}
		} catch (Exception e) {
			result.put("status", 0);
			result.put("message", e.getMessage());
			result.put("Object", null);
		}
		return result;
	}
	
	
	
}
