package id.co.indocyber.schedularRmi.common;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestUtil {
	public static RestResponse callRest(String url, Object sentObject, HttpMethod httpMethod) {
		url = "http://103.66.69.18:6026" + url;
		// url = url.replace("\\", "");
		RestTemplate restTemplate = new RestTemplate(); // UNTUK MENGKONSTRUKSI AWAL DI REST
		final HttpHeaders headers = new HttpHeaders(); // MEWAKILI REQUEST DAN RESPONSE DI HEADER
		headers.setContentType(MediaType.APPLICATION_JSON); // HEADER KONTEN DI SET MENJADI JSON
		final HttpEntity<Object> requestEntity = new HttpEntity<Object>(sentObject, headers);
		// MEMBUAT ENTITY HTTP BERBENTUK OBJECT DENGAN BODY DAN HEADER
		RestResponse restResponse = null;
		ResponseEntity<RestResponse> responseEntity = null;
		// EKSTENSI HTTPENTITY UNTUK MENAMBAHKAN HTTPSTATUS
		try {
			responseEntity = restTemplate.exchange(url, httpMethod, requestEntity, RestResponse.class);
			// EKSEKUSI HTTP METHOD DAN DI TAMPUNG DI RESPONSE ENTITY
			restResponse = responseEntity.getBody(); // MENAMPUNG BODY KE RESTRESPONSE
		} catch (Exception e) {
			restResponse = new RestResponse(0, null);
			e.printStackTrace();
		}
		return restResponse;
	}
	
}
