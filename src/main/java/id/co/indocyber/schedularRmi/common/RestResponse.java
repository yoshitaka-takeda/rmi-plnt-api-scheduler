package id.co.indocyber.schedularRmi.common;

import java.io.Serializable;

public class RestResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private Object status;
	private Object data;
	
	public RestResponse() {
	}
	
	public RestResponse(Object status, Object dataObject) {
		this.status = status;
		this.data = dataObject;
	}
	
	public Object getStatus() {
		return status;
	}
	
	public void setStatus(Object status) {
		this.status = status;
	}
	
	public Object getData() {
		return data;
	}
	
	public void setData(Object data) {
		this.data = data;
	}
}
