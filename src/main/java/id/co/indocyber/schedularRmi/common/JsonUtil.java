package id.co.indocyber.schedularRmi.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

public class JsonUtil {
	// METHOD INI MENAMPUNG LIST KE JSON
	public static <T> List<T> mapJsonToListObject(Object source, Class<T> class1) throws Exception {
		ObjectMapper mapper = new ObjectMapper(); // CONVERT JAVA OBJECT MENJADI
													// JSON
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		// MENGKONFIGURASI JIKA ADA KESALAHAN YANG TIDAK DIKETAHUI TERJADI
		String json = mapper.writeValueAsString(source); // MEMANGGIL JAVA VALUE
															// / NILAI KE BENTUK
															// STRING
		TypeFactory t = TypeFactory.defaultInstance(); // MENGKONSTRUKSI OBJECT
														// MAPPER SECARA DEFAULT
		List<T> list = mapper.readValue(json, t.constructCollectionType(ArrayList.class, class1));
		// MENAMPUNG MAPPER KE DALAM LIST
		// T DISINI DIGUNAKAN UNTUK TIPE PARAMETER YANG UMUMNYA DIPAKAI
		// T = Type , K = Key , V = value
		// https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/generics/index
		return list;
	}
	
	// METHOD INI MENAMPUNG MAP KE JSON
	public static <T, K> Map<T, K> mapJsonToHashMapObject(Object source) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String json = mapper.writeValueAsString(source);
		return mapper.readValue(json, new TypeReference<HashMap<T, K>>() {
		});
	}
	
	// METHOD INI MENAMPUNG OBJECT KE JSON
	public static <T> T mapJsonToSingleObject(Object source, Class<T> class1) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String json = mapper.writeValueAsString(source);
		return mapper.readValue(json, class1);
	}
	
	// METHOD INI UNTUK MENGAMBIL JSON
	public static String getJson(Object source) {
		if (source == null)
			return "";
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		String json = null;
		try {
			json = mapper.writeValueAsString(source);
			return json;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}
}
