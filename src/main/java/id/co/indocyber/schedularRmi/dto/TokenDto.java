package id.co.indocyber.schedularRmi.dto;

import java.util.List;

public class TokenDto {
	List<String> token;
	
	public List<String> getToken() {
		return token;
	}
	
	public void setToken(List<String> token) {
		this.token = token;
	}
	
}
