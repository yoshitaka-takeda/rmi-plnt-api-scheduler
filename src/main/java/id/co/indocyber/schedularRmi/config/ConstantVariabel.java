package id.co.indocyber.schedularRmi.config;

public class ConstantVariabel {
	public static final String _ipServer = "http://103.66.69.20:8080";
	public static final String _hostServer = "http://103.66.69.21:6026";
	
	public static final String _tokenSync = _hostServer + "/tokenservices/getresttoken";
	public static final String _listFarmer = _hostServer + "/caneservices/mobfarmer/list";
	public static final String _listFarm = _hostServer + "/caneservices/mobfarm/list";
	public static final String _listSpta = _hostServer + "/caneservices/mobspta/list";
	public static final String _listSptaWeightBridge = _hostServer + "/caneservices/mobsptaweightbridge/list";
	public static final String _listWeighBridgeResult = _hostServer + "/caneservices/mobweighbridgeresult/list";
	public static final String _listPaddyFieldCode = _hostServer + "/caneservices/mobpaddyfield/list";
	
	public static final String _URL = _ipServer + "/rmi-assets/rmi2-1ce51-firebase-adminsdk-xyvf6-7aeb060358.json";
	public static final String _DB = "https://rmi2-1ce51.firebaseio.com";
	
	public static final String _URLPost = "https://fcm.googleapis.com/fcm/send";
	public static final String _KEY = "AAAAATgf04Q:APA91bHQfYuMJuyEjq_U9AeSRIq1AGeqKDFbvTzl31W7Ir0EUiTiOn5yisgWcw-ZfBvJMF8nXuuGB7_w9CzusbGrvDctd4J2hIH_ItO_S4LSWpQr1h4ebNkjhv7kJR9Ou8C-5kENG9iU";
	public static final String _Authorization = "key=" + _KEY;
	public static final String _ContentType = "application/json";
	public static final String _BodySpta = "Anda mendapatkan spta baru dengan no : ";
	public static final String _BodyWbResult = "Hasil timbangan dengan no Spta : ";
	public static final String _Title = "RMI";
}
