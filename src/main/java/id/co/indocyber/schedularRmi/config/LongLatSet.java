package id.co.indocyber.schedularRmi.config;

public class LongLatSet {
	public String longLat(String data) {
		String longlat = "";
		String[] strings = data.split(",");
		longlat = strings[0] + "." + strings[1];
		return longlat;
	}
}
