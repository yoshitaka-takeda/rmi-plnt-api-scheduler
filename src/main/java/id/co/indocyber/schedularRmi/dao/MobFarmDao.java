package id.co.indocyber.schedularRmi.dao;

import id.co.indocyber.schedularRmi.entity.MobFarm;
import id.co.indocyber.schedularRmi.entity.MobFarmPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobFarmDao extends JpaRepository<MobFarm, MobFarmPK> {
	@Query("select count(a) from MobFarm a where a.farmCode = :farmCode") 
	public Long countDataByFarmCode(@Param("farmCode") String farmCode);
	
	@Query("select a from MobFarm a where a.farmerId = :farmerId and a.farmCode = :farmCode") 
	public MobFarm findOneByFarmCode(@Param("farmerId") Integer farmerId, @Param("farmCode") String farmCode);
}
