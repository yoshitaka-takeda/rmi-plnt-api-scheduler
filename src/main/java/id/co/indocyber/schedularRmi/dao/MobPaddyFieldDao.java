package id.co.indocyber.schedularRmi.dao;

import id.co.indocyber.schedularRmi.entity.MobPaddyField;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobPaddyFieldDao extends JpaRepository<MobPaddyField, Integer> {
	@Query("select count(a) from MobPaddyField a where a.farmerId = :farmerId and a.farmCode = :farmCode and a.paddyFieldCode = :paddyFieldCode ") 
	public Long countDataByFarmCodeAndPaddyFieldCode(@Param("farmerId") Integer farmerId, @Param("farmCode") String farmCode,@Param("paddyFieldCode") String paddyFieldCode);
	
	@Query("select a from MobPaddyField a where a.farmerId = :farmerId and a.farmCode = :farmCode and a.paddyFieldCode = :paddyFieldCode ") 
	public MobPaddyField findOneByFarmerFarmPaddyField(@Param("farmerId") Integer farmerId, @Param("farmCode") String farmCode, @Param("paddyFieldCode") String paddyFieldCode);
}
