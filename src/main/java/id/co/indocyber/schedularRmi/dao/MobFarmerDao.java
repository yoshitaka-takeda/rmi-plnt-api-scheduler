package id.co.indocyber.schedularRmi.dao;

import id.co.indocyber.schedularRmi.entity.MobFarmer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobFarmerDao extends JpaRepository<MobFarmer, Integer>{
	@Query("select count(a) from MobFarmer a where a.referenceCode = :referenceCode") 
	public Long countDataFarmerByRefCode(@Param("referenceCode") String referenceCode);
	
	@Query("select a from MobFarmer a where a.referenceCode = :referenceCode") 
	public MobFarmer findOneByRefCode(@Param("referenceCode") String referenceCode);
}
