package id.co.indocyber.schedularRmi.dao;

import id.co.indocyber.schedularRmi.entity.MobWeighbridgeResult;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobWeighbridgeResultDao extends JpaRepository<MobWeighbridgeResult, Integer>{

	@Query("select count(a) from MobWeighbridgeResult a where a.documentNum=:noSpta")
	public Long countWeighBridgeResultByid(@Param("noSpta") Integer noSpta);
	
	@Query("select a from MobWeighbridgeResult a where a.documentNum=:noSpta")
	public MobWeighbridgeResult getWeighbridgeResult(@Param("noSpta") Integer noSpta);
	
}
