package id.co.indocyber.schedularRmi.dao;


import id.co.indocyber.schedularRmi.entity.MobFarmer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface TokenUserDao extends JpaRepository<MobFarmer, Integer> {

	@Query(nativeQuery = true, value ="select b.token "
			+ "from mob_farmer a join m_user b on a.id = b.person_id "
			+ "where a.reference_code = :referenceCode") 
	public String findTokenPetani(@Param("referenceCode") String referenceCode);
	
	@Query(nativeQuery = true, value ="select d.token from "
			+ "mob_weighbridge_result as a join "
			+ "mob_spta as b on a.document_num = b.spta_num join "
			+ "mob_farmer as c on b.farmer_code = c.reference_code join "
			+ "m_user as d on c.id = d.person_id join "
			+ "m_user_role as e on d.email = e.email "
			+ "where e.role_id = 2 and a.document_num = :documentNum") 
	public String findTokenPetaniBySpta(@Param("documentNum") Integer documentNum);
	
	@Query(nativeQuery = true, value ="select b.token "
			+ "from mob_driver a join m_user b on a.id = b.person_id "
			+ "where a.id = :driverId") 
	public String findTokenSupir(@Param("driverId") Integer driverId);

	@Query(nativeQuery = true, value ="select d.token from "
			+ "mob_weighbridge_result as a join "
			+ "mob_spta as b on a.document_num = b.spta_num join "
			+ "mob_driver as c on b.driver_id = c.id join "
			+ "m_user as d on c.id = d.person_id join "
			+ "m_user_role as e on d.email = e.email "
			+ "where e.role_id = 3 and a.document_num = :documentNum") 
	public String findTokenSupirBySpta(@Param("documentNum") Integer documentNum);
	
	@Query(nativeQuery = true, value ="select b.token "
			+ "from mob_employee a join m_user b on a.id = b.person_id "
			+ "where a.id = :employeeId") 
	public String findTokenPetugas(@Param("employeeId") Integer employeeId);
}
