package id.co.indocyber.schedularRmi.dao;

import id.co.indocyber.schedularRmi.entity.MobSpta;
import id.co.indocyber.schedularRmi.entity.MobSptaPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobSptaDao extends JpaRepository<MobSpta, MobSptaPK>{

	@Query("select count(a) from MobSpta a where a.sptaNum=:noSpta")
	public Long countSptaByid(@Param("noSpta") Integer noSpta);
	
	@Query("select a from MobSpta a where a.sptaNum=:noSpta")
	public MobSpta getSptaBynoSpta(@Param("noSpta") Integer noSpta);
	
	@Query("select a.driverId from MobSpta a where a.sptaNum=:noSpta")
	public Integer getdriverId(@Param("noSpta") Integer noSpta);
}
