package id.co.indocyber.schedularRmi.entity;


import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the mob_farm database table.
 * 
 */
@Entity
@Table(name = "mob_farm")
@NamedQuery(name = "MobFarm.findAll", query = "SELECT m FROM MobFarm m")
@IdClass(MobFarmPK.class)
public class MobFarm implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "farmer_id")
	private int farmerId;

	@Id
	@Column(name = "farm_code")
	private String farmCode;

	@Column(name = "field_area")
	private BigDecimal fieldArea;

	@Column(name = "village")
	private String village;

	@Column(name = "sub_district")
	private String subDistrict;

	@Column(name = "district")
	private String district;

	@Column(name = "province")
	private String province;

	@Column(name = "latitude")
	private BigDecimal latitude;

	@Column(name = "longitude")
	private BigDecimal longitude;

	@Column(name = "farm_main_type")
	private String farmMainType;

	@Column(name = "farm_sub_type")
	private String farmSubType;

	@Column(name = "reference_code")
	private String referenceCode;

	@Column(name = "status")
	private String status;

	@Column(name = "is_active")
	private byte isActive;

	@Column(name = "processed_by")
	private String processedBy;

	@Column(name = "processed_date")
	private Date processedDate;

	public MobFarm() {
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public BigDecimal getFieldArea() {
		return fieldArea;
	}

	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getFarmMainType() {
		return farmMainType;
	}

	public void setFarmMainType(String farmMainType) {
		this.farmMainType = farmMainType;
	}

	public String getFarmSubType() {
		return farmSubType;
	}

	public void setFarmSubType(String farmSubType) {
		this.farmSubType = farmSubType;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

}