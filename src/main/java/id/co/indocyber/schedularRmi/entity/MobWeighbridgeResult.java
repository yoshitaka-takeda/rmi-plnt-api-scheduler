package id.co.indocyber.schedularRmi.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the mob_weighbridge_result database table.
 * 
 */
@Entity
@Table(name = "mob_weighbridge_result")
@NamedQuery(name = "MobWeighbridgeResult.findAll", query = "SELECT m FROM MobWeighbridgeResult m")
public class MobWeighbridgeResult implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private int id;
	
	@Column(name = "document_num")
	private int documentNum;
	
	@Column(name = "document_type")
	private String documentType;
	
	@Column(name = "mitra_code")
	private String mitraCode;
	
	@Column(name = "mitra_name")
	private String mitraName;
	
	@Column(name = "vehicle_no")
	private String vehicleNo;
	
	@Column(name = "vehicle_type")
	private String vehicleType;
	
	@Column(name = "driver_id")
	private String driverId;
	
	@Column(name = "driver_name")
	private String driverName;
	
	@Column(name = "farmer_code")
	private String farmerCode;
	
	@Column(name = "farmer_name")
	private String farmerName;
	
	@Column(name = "contract_no_1")
	private String contractNo1;
	
	@Column(name = "contract_no_2")
	private String contractNo2;
	
	@Column(name = "date_in")
	private Date dateIn;
	
	@Column(name = "time_in")
	private String timeIn;
	
	@Column(name = "date_out")
	private Date dateOut;
	
	@Column(name = "time_out")
	private String timeOut;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "monitoringPos")
	private String monitoringPos;
	
	@Column(name = "weigh_bruto")
	private BigDecimal weighBruto;
	
	@Column(name = "weigh_netto")
	private BigDecimal weighNetto;
	
	@Column(name = "weigh_vehicle")
	private BigDecimal weighVehicle;
	
	@Column(name = "qty_weigho")
	private BigDecimal qtyWeigho;
	
	@Column(name = "rafaksi")
	private BigDecimal rafaksi;
	
	@Column(name = "total_paid")
	private BigDecimal totalPaid;
	
	public MobWeighbridgeResult() {
	}
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public int getDocumentNum() {
		return documentNum;
	}
	
	public void setDocumentNum(int documentNum) {
		this.documentNum = documentNum;
	}
	
	public String getDocumentType() {
		return documentType;
	}
	
	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}
	
	public String getMitraCode() {
		return mitraCode;
	}
	
	public void setMitraCode(String mitraCode) {
		this.mitraCode = mitraCode;
	}
	
	public String getMitraName() {
		return mitraName;
	}
	
	public void setMitraName(String mitraName) {
		this.mitraName = mitraName;
	}
	
	public String getVehicleNo() {
		return vehicleNo;
	}
	
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	
	public String getVehicleType() {
		return vehicleType;
	}
	
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	
	public String getDriverId() {
		return driverId;
	}
	
	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
	
	public String getDriverName() {
		return driverName;
	}
	
	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}
	
	public String getFarmerCode() {
		return farmerCode;
	}
	
	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}
	
	public String getFarmerName() {
		return farmerName;
	}
	
	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}
	
	public String getContractNo1() {
		return contractNo1;
	}
	
	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}
	
	public String getContractNo2() {
		return contractNo2;
	}
	
	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}
	
	public Date getDateIn() {
		return dateIn;
	}
	
	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}
	
	public String getTimeIn() {
		return timeIn;
	}
	
	public void setTimeIn(String timeIn) {
		this.timeIn = timeIn;
	}
	
	public Date getDateOut() {
		return dateOut;
	}
	
	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}
	
	public String getTimeOut() {
		return timeOut;
	}
	
	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMonitoringPos() {
		return monitoringPos;
	}
	
	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}
	
	public BigDecimal getWeighBruto() {
		return weighBruto;
	}
	
	public void setWeighBruto(BigDecimal weighBruto) {
		this.weighBruto = weighBruto;
	}
	
	public BigDecimal getWeighNetto() {
		return weighNetto;
	}
	
	public void setWeighNetto(BigDecimal weighNetto) {
		this.weighNetto = weighNetto;
	}
	
	public BigDecimal getWeighVehicle() {
		return weighVehicle;
	}
	
	public void setWeighVehicle(BigDecimal weighVehicle) {
		this.weighVehicle = weighVehicle;
	}
	
	public BigDecimal getQtyWeigho() {
		return qtyWeigho;
	}
	
	public void setQtyWeigho(BigDecimal qtyWeigho) {
		this.qtyWeigho = qtyWeigho;
	}
	
	public BigDecimal getRafaksi() {
		return rafaksi;
	}
	
	public void setRafaksi(BigDecimal rafaksi) {
		this.rafaksi = rafaksi;
	}
	
	public BigDecimal getTotalPaid() {
		return totalPaid;
	}
	
	public void setTotalPaid(BigDecimal totalPaid) {
		this.totalPaid = totalPaid;
	}
	
}