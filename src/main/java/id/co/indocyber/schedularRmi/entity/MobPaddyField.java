package id.co.indocyber.schedularRmi.entity;

import java.io.Serializable;

import javax.persistence.*;

import java.math.BigDecimal;
import java.util.Date;

/**
 * The persistent class for the mob_paddy_field database table.
 * 
 */
@Entity
@Table(name = "mob_paddy_field")
@NamedQuery(name = "MobPaddyField.findAll", query = "SELECT m FROM MobPaddyField m")
public class MobPaddyField implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name = "farmer_id")
	private int farmerId;

	@Column(name = "farm_code")
	private String farmCode;

	@Column(name = "paddy_field_code")
	private String paddyFieldCode;

	@Column(name = "line_num")
	private int lineNum;

	@Column(name = "field_area")
	private BigDecimal fieldArea;

	@Column(name = "latitude")
	private BigDecimal latitude;

	@Column(name = "longitude")
	private BigDecimal longitude;

	@Column(name = "weight_estimation")
	private BigDecimal weightEstimation;

	@Column(name = "sugarcane_type")
	private String sugarcaneType;

	@Column(name = "age_of_sugarcane")
	private int ageOfSugarcane;

	@Column(name = "planting_date")
	private Date plantingDate;

	@Column(name = "sugarcane_species")
	private String sugarcaneSpecies;

	@Column(name = "sugarcane_score")
	private BigDecimal sugarcaneScore;

	@Column(name = "sugarcane_qty")
	private BigDecimal sugarcaneQty;

	@Column(name = "planting_period")
	private String plantingPeriod;

	@Column(name="weigh_estimation")
	private BigDecimal weighEstimation;
	
	@Column(name="status")
	private String status;
	
	@Column(name = "reference_code")
	private String referenceCode;

	@Column(name = "is_active")
	private byte isActive;

	@Column(name = "processed_by")
	private String processedBy;

	@Column(name = "processed_date")
	private Date processedDate;

	public MobPaddyField() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(int farmerId) {
		this.farmerId = farmerId;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public int getLineNum() {
		return lineNum;
	}

	public void setLineNum(int lineNum) {
		this.lineNum = lineNum;
	}

	public BigDecimal getFieldArea() {
		return fieldArea;
	}

	public void setFieldArea(BigDecimal fieldArea) {
		this.fieldArea = fieldArea;
	}

	public BigDecimal getLatitude() {
		return latitude;
	}

	public void setLatitude(BigDecimal latitude) {
		this.latitude = latitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public BigDecimal getWeightEstimation() {
		return weightEstimation;
	}

	public void setWeightEstimation(BigDecimal weightEstimation) {
		this.weightEstimation = weightEstimation;
	}

	public String getSugarcaneType() {
		return sugarcaneType;
	}

	public void setSugarcaneType(String sugarcaneType) {
		this.sugarcaneType = sugarcaneType;
	}

	public int getAgeOfSugarcane() {
		return ageOfSugarcane;
	}

	public void setAgeOfSugarcane(int ageOfSugarcane) {
		this.ageOfSugarcane = ageOfSugarcane;
	}

	public Date getPlantingDate() {
		return plantingDate;
	}

	public void setPlantingDate(Date plantingDate) {
		this.plantingDate = plantingDate;
	}

	public String getSugarcaneSpecies() {
		return sugarcaneSpecies;
	}

	public void setSugarcaneSpecies(String sugarcaneSpecies) {
		this.sugarcaneSpecies = sugarcaneSpecies;
	}

	public BigDecimal getSugarcaneScore() {
		return sugarcaneScore;
	}

	public void setSugarcaneScore(BigDecimal sugarcaneScore) {
		this.sugarcaneScore = sugarcaneScore;
	}

	public BigDecimal getSugarcaneQty() {
		return sugarcaneQty;
	}

	public void setSugarcaneQty(BigDecimal sugarcaneQty) {
		this.sugarcaneQty = sugarcaneQty;
	}

	public String getPlantingPeriod() {
		return plantingPeriod;
	}

	public void setPlantingPeriod(String plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public byte getIsActive() {
		return isActive;
	}

	public void setIsActive(byte isActive) {
		this.isActive = isActive;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public BigDecimal getWeighEstimation() {
		return weighEstimation;
	}

	public void setWeighEstimation(BigDecimal weighEstimation) {
		this.weighEstimation = weighEstimation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}