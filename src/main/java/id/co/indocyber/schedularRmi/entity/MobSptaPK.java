package id.co.indocyber.schedularRmi.entity;

import java.io.Serializable;

public class MobSptaPK implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int sptaNum;

	public int getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(int sptaNum) {
		this.sptaNum = sptaNum;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + sptaNum;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobSptaPK other = (MobSptaPK) obj;
		if (sptaNum != other.sptaNum)
			return false;
		return true;
	}

}
